FROM webdevops/php-nginx:8.1-alpine
RUN apk add oniguruma-dev mysql-dev libxml2-dev

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ENV WEB_DOCUMENT_ROOT /app/public
ENV APP_ENV production
WORKDIR /app
COPY . .

RUN composer install --no-interaction --optimize-autoloader --no-dev
RUN chown -R www-data:www-data .


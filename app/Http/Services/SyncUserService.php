<?php

namespace App\Http\Services;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;

class SyncUserService
{
    public const PREFIX_REDIS_KEY = "USERS_KEY";
    public const REDIS_EXPIRE_PARAM = 'EX';
    public const REDIS_EXPIRE_TTL = 3600;
    public const USER_COUNT = 5;

    /**
     * @return Collection<User>
     */
    public function sync(): Collection
    {
        $users = User::factory()->count(self::USER_COUNT)->create();
        Redis::set(
            self::PREFIX_REDIS_KEY,
            $users->toJson(),
            self::REDIS_EXPIRE_PARAM,
            self::REDIS_EXPIRE_TTL
        );
        return $users;
    }
}